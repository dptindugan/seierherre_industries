<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use App\User;
use Str;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        $this->authorize('notGuest', $project);

        $projects = Project::all();
        $users = User::all();
        return view('projects.index',[

            'projects' => $projects,
            'users' => $users
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project)
    {
        $this->authorize('notGuest', $project);
        $this->authorize('create', $project);
        $managers = User::where('role_id', 3)->get();
        // dd($managers);
        return view('projects.create')->with('managers',$managers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        // dd($random_char);
      $this->authorize('isAdmin', $project);
      $this->authorize('notGuest', $project);

        
        echo "andito ka";
        $request->validate([
            'name' => 'required',
            'details' => 'required',
            'projectManager' => 'required'
        ]);

        $data = $request->all();
        extract($data);

        $firstLetters = [];

        $newName = explode(" ",$name);
        
        $nameStr = $newName[0];

        // newName is an array of names
        // get the length of newName if the length is greaterthan 1
        // take the first letters of each names starting from index 1

        if (count($newName) > 1) {
            
            for ($i=1; $i < count($newName); $i++) { 
                 
                $names = $newName[$i];
                array_push($firstLetters, $names[0]);
            }
        }

        $firstLetters = implode("",$firstLetters);
        // dd($firstLetters);

        $nameStr = strtoupper($nameStr);
        $firstLetters = strtoupper($firstLetters);

        $random_char = strtoupper(Str::random(2));


        $project = new Project;
        $project->name = $name;
        $project->project_code = $nameStr.$firstLetters.$random_char."_".date('Ymd');
        $project->details = $details;
        $project->project_manager_id = $projectManager;
        $project->save();

        $request->session()->flash('createProjectSuccess','Successfully Added A Project');
        return redirect(route('projects.show',['project' => $project->id]));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $this->authorize('isAdmin', $project);
        $users = User::where('role_id',3)->get();
        return view('projects.show',[
            'project' => $project,
            'users' => $users
        ]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $this->authorize('isAdmin', $project);

        $users = User::where('role_id',3)->get();
        return view('projects.edit',
            [
                'project'=>$project,
                'users' => $users
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $this->authorize('isAdmin', $project);

        $request->validate([
            'name' => 'required',
            'details' => 'required',
            'projectManager' => 'required'
        ]);

        $data = $request->all();
        extract($data);

        if ($project->name == $name && $project->details == $details
            && $project->project_manager_id == $projectManager) {
            $request->session()->flash('update_failed','No changes made');
            return redirect(route('projects.show',['product'=> $product->id]));
        }
        else
        {


            $firstLetters = [];

            $newName = explode(" ",$name);
            
            $nameStr = $newName[0];

            // newName is an array of names
            // get the length of newName if the length is greaterthan 1
            // take the first letters of each names starting from index 1

            if (count($newName) > 1) {
                
                for ($i=1; $i < count($newName); $i++) { 
                     
                    $names = $newName[$i];
                    array_push($firstLetters, $names[0]);
                }
            }

            $firstLetters = implode("",$firstLetters);
            // dd($firstLetters);

            $nameStr = strtoupper($nameStr);
            $firstLetters = strtoupper($firstLetters);

            $random_char = strtoupper(Str::random(2));

            $project->name = $name;
            $project->details = $details;
            $project->project_manager_id = $projectManager;
            $project->save();

            $request->session()->flash('updateProjectSuccess','Project Successfully Updated');
            return redirect(route('projects.show',['project' => $project->id]));

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $this->authorize('isAdmin', $project);
        
        $project->delete();

        return redirect(route('projects.index'))->with('destroy_message', 'Project Deleted');
    }
}
