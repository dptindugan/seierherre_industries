<?php

namespace App\Http\Controllers;

use App\ResourceCategory;
use Illuminate\Http\Request;

class ResourceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ResourceCategory $resourceCategory)
    {
        $this->authorize('isAdmin', $resourceCategory);

        // echo " ako to";
        $resourceCategory = ResourceCategory::all();
        return view('categories.index')->with('resource_categories',$resourceCategory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ResourceCategory $resourceCategory)
    {
        $this->authorize('isAdmin', $si_resource);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ResourceCategory $resourceCategory)
    {   
        // echo "hellooo";
            $this->authorize('isAdmin', $resourceCategory);

            echo "hindi akoto";
            $request->validate([
                'add-category' => "required|unique:resource_categories,name"
            ]);
            $category = $request->input('add-category');
            $new_category = new ResourceCategory;
            // dd($new_category);
            $new_category->name = $category;
            $new_category->save();
            $request->session()->flash('category_message','Category successfully added!');

        if ($request->has('addCategoryModal')) {
            // echo "ako to";
            return redirect(route('resource_categories.index'));
        } else{
            echo 'hindi ako to';
            return redirect(route('si_resource.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResourceCategory  $resourceCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ResourceCategory $resourceCategory)
    {
        $this->authorize('isAdmin', $resourceCategory);

        // return view('resource_category')->with('resource_category',$resourceCategory);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResourceCategory  $resourceCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ResourceCategory $resourceCategory)
    {
        $this->authorize('update', $resourceCategory);

        // return view('')->with('resource_category',$resourceCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResourceCategory  $resourceCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResourceCategory $resourceCategory)
    {
        echo "wassap";

        $this->authorize('update', $resourceCategory);

        // echo "echo mo to";
        $request->validate([
            'categoryName' => 'required'
        ]);

        if($resourceCategory->name == $request->input('categoryName')){
            $request->session()->flash('update_failed', 'No changes made');
            return redirect(route('resource_categories.index',['resource_category'=>$resourceCategory->id]));
        }
        else
        {
            $request->validate([
                'categoryName' => 'required'
            ]);

            $resourceCategory->name = $request->input('categoryName');
            $resourceCategory->save();

            $request->session()->flash('update_success', 'Category Successfully Updated!');
            return redirect(route('resource_categories.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResourceCategory  $resourceCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ResourceCategory $resourceCategory)
    {
        $this->authorize('delete', $resourceCategory);

        echo "eto ay delete";
        $resourceCategory->delete();

        $request->session()->flash('delete_success', 'Category has been deleted!');
        return redirect(route('resource_categories.index'));
    }

    public function deleteAll(ResourceCategory $resourceCategory)
    {
        $this->authorize('delete', $resourceCategory);

        echo "eto ay delete all";
        $resourceCategory = ResourceCategory::all();

        foreach ($resourceCategory as $category) {
            // echo $category->name;
            // dd($category);
            $category->delete();
        }

        // $resourceCategory->delete();

        return redirect(route('resource_categories.index'))->with('deleteAll_success', 'All categories has been deleted!');
    }
}
