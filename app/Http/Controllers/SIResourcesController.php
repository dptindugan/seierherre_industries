<?php

namespace App\Http\Controllers;

use App\SI_resources;
use Illuminate\Http\Request;
use Str;
use App\ResourceCategory;

class SIResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SI_resources $si_resource)
    {   
        $this->authorize('notGuest', $si_resource);
        
        $si_resources = SI_resources::all();
        return view('SI_resources.index',[
            'si_resource' => $si_resources 
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SI_resources $si_resource)
    {
        $this->authorize('create', $si_resource);

        $categories = ResourceCategory::all();
        return view('SI_resources.create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SI_resources $si_resource)
    {

        $this->authorize('create', $si_resource);

        $request->validate([
            'name' => 'required',
            'details' => 'required',
            'image'=> 'required|image|max:3000',
            'stocks' => 'required'
        ]);

        // echo "Hello World";

        $file = $request->file('image');


        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $file_extension = $file->extension();

        $random_chars = Str::random(10);
        $random_char2 = Str::random(2);

        $regex = "/[\-\ \_]/";
        $fileNameLetters = [];

        if (count_chars($file_name) > 7) {
            if(preg_match($regex, $file_name)){
                $file_nameArr = preg_split($regex, $file_name);
                // dd($file_nameArr);

                for ($i=0; $i < count($file_nameArr); $i++) { 
                    $fileName = $file_nameArr[$i];
                    // dd($fileName);
                    empty($fileName)? "" : array_push($fileNameLetters, $fileName[0]);
                    // dd($fileNameLetters);
                }
                
                $file_name = implode("", $fileNameLetters);
                $new_file_name = date('Y-m-d-H-i-s')."_".$random_chars."_".$file_name.".".$file_extension;
                // dd($new_file_name);

                
            }
            else
            {
                for ($i=0; $i <= 7 ; $i++) { 
                    array_push($fileNameLetters, $fileName[$i]);
                }
                $file_name = implode("", $fileNameLetters);
                $new_file_name = date('Y-m-d-H-i-s')."_".$random_chars."_".$file_name.".".$file_extension;


            }

        } else{

            $new_file_name = date('Y-m-d-H-i-s')."_".$random_chars."_".$file_name.".".$file_extension;
        }



        $filepath = $file->storeAs('images',$new_file_name,'public');

        $data = $request->all(
            'name',
            'category',
            'details',
            'stocks'
        );

        extract($data);
        $newName = explode(" ",$name);
        $newName = implode("_",$newName);

        if ($category == 1) {
            $controlNumber = $newName."_".$random_char2."_".date('Ymd');
        }else{
            $controlNumber = $newName."_".date('Ymd');
        }

        $resources = new SI_resources;
        $resources->name = $name;
        $resources->category_id = $category;
        $resources->details = $details;
        $resources->images = $filepath;
        $resources->control_number = $controlNumber;
        $resources->stocks = $stocks;
        $resources->save();

        return redirect(route('si_resource.show',['si_resource'=> $resources->id]));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\SI_resources  $sI_resources
     * @return \Illuminate\Http\Response
     */
    public function show(SI_resources $si_resource)
    {
        // dd($sI_resources);
        // echo "hallooo!!";
        $this->authorize('create', $si_resource);

        return view('SI_resources.show')->with('si_resource', $si_resource);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SI_resources  $sI_resources
     * @return \Illuminate\Http\Response
     */
    public function edit(SI_resources $si_resource)
    {
        $this->authorize('update', $si_resource);

        $categories = ResourceCategory::all();
        return view('SI_resources.create',[
            'si_resource' => $si_resource,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SI_resources  $sI_resources
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SI_resources $si_resource)
    {
        $this->authorize('update', $si_resource);

        // dd($request->input('details'));

        $request->validate([
            'name'=>'required|string',
            // 'image'=> 'required|image|max:3000',
            'details' => 'required|string'

        ]);

        // 2. update database if there are differences between the details from form and the current details of the product;
        if (!$request->hasFile('image') &&
            $si_resource->name == $request->input('name') &&
            $si_resource->details == $request->input('details')&&
            $si_resource->category_id == $request->input('category')) {
            $request->session()->flash('update_failed','No changes made');
        return redirect(route('si_resource.edit',['si_resource'=> $si_resource->id]));
    }else{

            // update the entry in the database and return the update entry

            //check if there's a file uploaded
     if($request->hasFile('image')){
       $request->validate([
         'image' => 'required|image|max:3000'
     ]);
       $file = $request->file('image');
       $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
       $file_extension = $file->extension();
       $random_chars = Str::random(10);
       $new_file_name = date('Y-m-d-H-i-s')."_".$random_chars."_".$file_name.".".$file_extension;
       $filepath = $file->storeAs('images',$new_file_name,'public');

       $si_resource->images=$filepath;
   }

                // $product = new Product;
   $si_resource->name = $request->input('name');
   $si_resource->category_id = $request->input('category');
   $si_resource->details = $request->input('details');
   $si_resource->save();

   $request->session()->flash('update_success', 'Product successfully updated');

   return redirect(route('si_resource.show',['si_resource'=> $si_resource->id]));
}
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SI_resources  $sI_resources
     * @return \Illuminate\Http\Response
     */
    public function destroy(SI_resources $si_resource)
    {
        $this->authorize('delete', $si_resource);

        // echo "dapat mawawala to";
        $si_resource->delete();

        // $request->session()->flash('delete_success', 'Product successfully deleted');
        return redirect(route('si_resource.index'))->with('destroy_message','Resource Successfully Deleted');
    }
    
}
