<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SI_resources;
use Str;
use Session;
use App\Project;

class TempController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('notGuest');

        $projects = Project::all();
        $resources = [];
        if(Session::has('temp')){
            $resource_ids = array_keys(Session::get('temp'));
            // dd($resource);

            $resources = SI_resources::find($resource_ids);
            foreach ($resources as $resource) {
                $resource['quantity'] = Session::get("temp.$resource->id");
            }

            return view('temp.index',[
                'si_resources' => $resources,
                'projects' => $projects
            ]);
        }

        
        return view('temp.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('notGuest');

        $resources = SI_resources::all();
        $inputs = $request->all();
        // dd($inputs);
        
        // for each inputs if the check box value is on then check in their name(id) is equals to quantity id
        // dd($inputs);
        foreach ($inputs as $input => $value) {
            if($value == 'on'){
                foreach($inputs as $quantityName => $quantityValue){
                    if("quantity".$input == $quantityName){
                        echo $quantityName ." = " .$quantityValue;

                        $request->validate([
                            $quantityName => 'required|numeric|min:1'
                        ]);

                        $request->session()->put("temp.$input",$quantityValue);
                    }
                }
            }
        }

                        // dd(Session::get('temp'));

                        return redirect(route('temp.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('notGuest', $id);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('notGuest', $id);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('notGuest', $id);

        $request->validate([
         'id' => 'required',
         'quantity' => 'required|numeric|min:1'
      ]);


        //get the request input
       $item_quantity = $request->input('quantity');
       $id = $request->input('id');

        //set entry to be pass to session
        // $add_to_cart = [$id => $product_quantity];
        // store to session
        // $request->session()->put("cart",[$add_to_cart]);
       $request->session()->put("temp.$id",$item_quantity);
       return redirect(route('temp.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('notGuest', $id);

        Session::forget("temp.$id");

       return redirect(route('temp.index'));
    }

    public function clearTemp()
    {
        $this->authorize('notGuest', $id);
        
       Session::forget("temp");
       // echo "Empty Cart Button is Clicked";
       return redirect(route('temp.index'));
    }
}
