<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;
use App\Providers\AuthServiceProvider;
use Auth;
use Session;
use str;
use App\Project;
use App\ProjectStatus;
use App\TicketStatus;
use App\SI_resources;

class TicketController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Ticket $ticket)
    {

        $this->authorize('notGuest', $ticket);
        

        if(strtolower(Auth::user()->role->name) == 'admin'){
            $tickets = Ticket::where('ticket_status_id', '!=', 1)->get();
            $ticket_statuses = TicketStatus::all();
            // dd($tickets);
        } 

        else if(strtolower(Auth::user()->role->name) == 'manager')
        {
            $tickets = Ticket::where('manager_user_id', Auth::user()->id)->get();
            $ticket_statuses = TicketStatus::all();
            
        }

        else if (strtolower(Auth::user()->role->name) == 'audit') {

            $tickets = Ticket::whereIn('ticket_status_id', [3,4])->get();
            // dd($tickets);
            // array_push($tickets, $completedTickets) ;
            $ticket_statuses = TicketStatus::all();
        }

        else
        {

            $tickets = Ticket::where('user_id', Auth::user()->id)->get();
            $ticket_statuses = TicketStatus::all();

        }



        return view('tickets.index',[
            'tickets' => $tickets,
            'ticket_statuses' => $ticket_statuses
        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Ticket $ticket)
    {
        $this->authorize('notGuest', $ticket);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Ticket $ticket)
    {
        // echo "mag store ka!!";
        $this->authorize('notGuest', $ticket);


        $request->validate([
            'project' => 'required'
        ]);
        $project = Project::where('id', $request->input('project'))->get();

        if (count($project) && count($project) == 1) {
            $project = $project[0];
        }

        if ($project->project_status_id == 2 || $project->project_status_id == 3) {
            
            $message_status = $project->project_status_id == 2 ? 'completed' : 'cancelled'; 
            return view('temp.index')->with('project_status_message', "Project is $message_status <br> Please select another project");
            
        }

        else

        {
            $tickets = new Ticket;

            // create a the reference number using the user name's initials and id, the project id then the date
            $projectId = $request->input('project');
            $user = Auth::user();
            $userName = $user->name;
            $userId = $user->id;
            $userNameArr = explode(" ",$userName);
            $userInitials = [];

            if (count($userNameArr) > 1) {
                for ($i=1; $i < count($userNameArr); $i++) { 
                    $userName = $userNameArr[$i];
                    array_push($userInitials, $userName[0]);
                    $userInitials = implode("",$userInitials);
                }
            } else{

                $userInitials = $userName[0];
            }

            // echo $userInitials;

            // dd($username);

            $reference_no = $userInitials.$userId."PID".$projectId.Str::random(3)."_".date('Ymd-hi')."_";
            
            // put the $reference_no to the table
            $tickets->reference_no = $reference_no;

            $tickets->project_id = $projectId;

            $tickets->user_id = $userId;

            // echo $user->role->name;
            if (strtolower($user->role->name) == 'manager')  {

                $tickets->ticket_status_id = 2;

            } else if(strtolower($user->role->name) == 'employee'){

                $tickets->manager_user_id = $user->manager_user_id;
            }

            $tickets->save();

            // return 

            $temp_ids = array_keys(Session::get('temp'));
            $resources = SI_resources::find($temp_ids);

            // dd($resources);

            foreach (Session::get('temp') as $request_id => $quantity) {
                foreach ($resources as $resource) {
                    if ($resource->id == $request_id) {
                        $tickets->resources()
                        ->attach($resource->id,
                            [
                                'quantity' => $quantity,
                            ]
                        );
                    }
                }
            }

            $tickets->save();

            Session::forget('temp');

            return redirect(route('tickets.show',['ticket' => $tickets->id]));
            // echo $reference_no;
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        $this->authorize('notGuest', $ticket);
        
        // dd($ticket->project_id);
        // $project = Project::where('id', $ticket->project_id)->get();
        // dd($project);
        return view('tickets.show')->with('ticket',$ticket)/*->with('project' , $project[0])*/;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        $this->authorize('notGuest', $ticket);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        $this->authorize('update', $ticket);

        $request->validate([
            'action_btn' => 'required'
        ]);

        // echo "you have been ".$request->input('action_btn');
        $action = $request->input('action_btn');

        // check who is the user
        // check the value of the btn if value is approved set status_id to approved(user is admin), processed(user is manager), if rejected by (manager doesn't proceed to admin(status rejected))

        // note that unapproved status by the manager cannot be seen by the admin
        // every processed requests(created by admin and/or created by user that is approved by admin)
        $userName = strtolower(Auth::user()->role->name);

        if ($userName == 'admin') {
            // if action button value is approved
            if ($action == 'approved') {
                $ticket->ticket_status_id = 3;
                $ticket->save();
            }
            else if ($action == 'rejected') {
                $ticket->ticket_status_id = 5;
                $ticket->save();
            }

            return redirect(route('tickets.index'))
            ->with('updated_ticket', $ticket->id);
        }

        else if($userName == 'manager'){
            if ($action == 'approved') {
                $ticket->ticket_status_id = 2;
                $ticket->save();
            }
            else if ($action == 'rejected') {
                $ticket->ticket_status_id = 5;
                $ticket->save();
            }
            return redirect(route('tickets.index'))
            ->with('updated_ticket', $ticket->id);
            
        }

        else if($userName == 'audit'){
            if ($action == 'completed'){
                // $ticket->ticket_status_id = 4;
                // $ticket->save();

                // echo "adito ako";
                // dd($ticket->id);
                $tickets = Ticket::where('id' ,$ticket->id)->get();
                // $resouces = SI_resources::all()
                
                foreach ($tickets as $finder) {
                    foreach ($finder->resources as $resourceFinder) {

                        $resources = SI_resources::where('id', $resourceFinder->id)->get();
                        // dd($resources);

                        if (null != $resources){
                            $resources = $resources[0];
                            // echo $resources->stocks . "<br>";
                            $newStocksValue = $resources->stocks - $resourceFinder->pivot->quantity;

                            if ($newStocksValue >= 0) {
                                
                                SI_resources::where('id', $resourceFinder->id)->update(['stocks' => $newStocksValue]);
                                $ticket->ticket_status_id = 4;
                                $ticket->save();
                                return redirect(route('tickets.index'))
                                ->with('updated_ticket', $ticket->id);
                            }

                            else{
                                return redirect(route('tickets.index'))
                                ->with('update_error', "Resource out of Stock <br> Please contact the Admin");
                            }
                        }

                        // echo $resourceFinder->pivot->quantity. "<br>";
                        // echo $resourceFinder->name ."<br>";
                        // echo $resourceFinder->id ."<br>";

                    }
                }
                // dd($ticket->resources->quantity);

            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        $this->authorize('notGuest', $ticket);
        //
    }

    public function resourceRequestUpdate(Ticket $ticket)
    {
        $this->authorize('notGuest', $ticket);
        
    }
}
