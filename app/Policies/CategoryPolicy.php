<?php

namespace App\Policies;

use App\ResourceCategory;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any resoure categories.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return strtolower($user->role->name) == 'admin';
        
    }

    /**
     * Determine whether the user can view the resoure category.
     *
     * @param  \App\User  $user
     * @param  \App\ResoureCategory  $resoureCategory
     * @return mixed
     */
    public function view(User $user, ResourceCategory $resourceCategory)
    {
        return strtolower($user->role->name) == 'admin';
        
    }

    /**
     * Determine whether the user can create resoure categories.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return strtolower($user->role->name) == 'admin';
    }

    /**
     * Determine whether the user can update the resoure category.
     *
     * @param  \App\User  $user
     * @param  \App\ResoureCategory  $resoureCategory
     * @return mixed
     */
    public function update(User $user, ResourceCategory $resourceCategory)
    {
        return strtolower($user->role->name) == 'admin';
        // return true;
        
    }

    /**
     * Determine whether the user can delete the resoure category.
     *
     * @param  \App\User  $user
     * @param  \App\ResoureCategory  $resoureCategory
     * @return mixed
     */
    public function delete(User $user, ResourceCategory $resourceCategory)
    {
        return strtolower($user->role->name) == 'admin';
        
    }

    /**
     * Determine whether the user can restore the resoure category.
     *
     * @param  \App\User  $user
     * @param  \App\ResoureCategory  $resoureCategory
     * @return mixed
     */
    public function restore(User $user, ResourceCategory $resourceCategory)
    {
        return strtolower($user->role->name) == 'admin';
        
    }

    /**
     * Determine whether the user can permanently delete the resoure category.
     *
     * @param  \App\User  $user
     * @param  \App\ResoureCategory  $resoureCategory
     * @return mixed
     */
    public function forceDelete(User $user, ResourceCategory $resourceCategory)
    {
        return strtolower($user->role->name) == 'admin';
        
    }
}
