<?php

namespace App\Policies;

use App\SI_resources;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ResourcePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any s i_resources.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the s i_resources.
     *
     * @param  \App\User  $user
     * @param  \App\SI_resources  $sIResources
     * @return mixed
     */
    public function view(User $user, SI_resources $si_resource)
    {
        //
    }

    /**
     * Determine whether the user can create s i_resources.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return strtolower($user->role->name) == 'admin';
    }

    /**
     * Determine whether the user can update the s i_resources.
     *
     * @param  \App\User  $user
     * @param  \App\SI_resources  $si_resource
     * @return mixed
     */
    public function update(User $user, SI_resources $si_resource)
    {
        return strtolower($user->role->name) == 'admin';
    }

    /**
     * Determine whether the user can delete the s i_resources.
     *
     * @param  \App\User  $user
     * @param  \App\SI_resources  $si_resource
     * @return mixed
     */
    public function delete(User $user, SI_resources $si_resource)
    {
        return strtolower($user->role->name) == 'admin';
    }

    /**
     * Determine whether the user can restore the s i_resources.
     *
     * @param  \App\User  $user
     * @param  \App\SI_resources  $sIResources
     * @return mixed
     */
    public function restore(User $user, SI_resources $si_resource)
    {
        return strtolower($user->role->name) == 'admin';
    }

    /**
     * Determine whether the user can permanently delete the s i_resources.
     *
     * @param  \App\User  $user
     * @param  \App\SI_resources  $sIResources
     * @return mixed
     */
    public function forceDelete(User $user, SI_resources $si_resource)
    {
        //
    }
}
