<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use App\User;

class Project extends Model
{
    use SoftDeletes;
    public function user(){
    	return $this->belongsTo('App\User');
    	// return 'waw';
    }

    public function projectManager(){
        return $this->belongsTo('App\User');
    }

    public function employee(){
        return $this->hasMany('App\User');
    }

    public function projectStatus(){
    	return $this->belongsTo('App\ProjectStatus');
    }

    public function tickets(){
    	return $this->hasMany('App\Tickets');
    }
}
