<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        'App\Project' => 'App\Policies\ProjectPolicy',
        'App\SI_resources' => 'App\Policies\ResourcePolicy',
        'App\ResourceCategory' => 'App\Policies\CategoryPolicy',
        'App\Ticket' => 'App\Policies\TicketPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function($user){
            return strtolower($user->role->name) == 'admin';
        });

        Gate::define('isManager', function($user){
            return strtolower($user->role->name) == 'manager';
        });

        Gate::define('isEmployee', function($user){
            return strtolower($user->role->name) == 'employee';
        });

        Gate::define('isAudit', function($user){
            return strtolower($user->role->name) == 'audit';
        });

        Gate::define('notGuest', function($user){
            return strtolower($user->role->name) !== null;
        });
    }
}
