<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ResourceCategory extends Model
{
	use SoftDeletes;
    public function resources(){
    	return $this->belongsTo('App\SI_resources');
    }

}
