<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SI_resources extends Model
{
    public function category(){
    	return $this->belongsTo('App\ResourceCategory');
    }

    // public function tickets(){
    // 	return $this->belongsToMany('App\Ticket');
    // }

    protected $fillable = ['name', 'details', 'category_id', 'images'];
}
