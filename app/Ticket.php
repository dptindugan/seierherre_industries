<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function project(){
    	return $this->belongsTo('App\Project');
    }
    
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function projectManager(){
    	return $this->belongsTo('App\User');
    }

    public function ticketStatus(){
    	return $this->belongsTo('App\TicketStatus');
    }

    public function resources(){
    	return $this->belongsToMany('App\SI_resources', 'resource_ticket')
    			->withPivot('quantity')->withTimestamps();
    }


}
