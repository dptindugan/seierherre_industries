<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('project_code');
            $table->mediumText('details');

            $table->unsignedBigInteger('project_status_id')->default(1);
            $table->foreign('project_status_id')
                    ->references('id')
                    ->on('project_statuses')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');

            $table->unsignedBigInteger('project_manager_id');
            $table->foreign('project_manager_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');

            // $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
