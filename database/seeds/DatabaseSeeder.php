<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(ProjectStatusSeeder::class);
        $this->call(RequestStatusSeeder::class);
        $this->call(ResourceCategoriesTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
    }
}
