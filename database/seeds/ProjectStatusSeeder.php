<?php

use Illuminate\Database\Seeder;

class ProjectStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_statuses')->insert(['name' => 'On Going']);
        DB::table('project_statuses')->insert(['name' => 'Completed']);
        DB::table('project_statuses')->insert(['name' => 'Cancelled']);
    }
}
