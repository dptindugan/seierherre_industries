<?php

use Illuminate\Database\Seeder;

class RequestStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ticket_statuses')->insert(['name' => 'Pending']);
        DB::table('ticket_statuses')->insert(['name' => 'Processed']);
        DB::table('ticket_statuses')->insert(['name' => 'Approved']);
        DB::table('ticket_statuses')->insert(['name' => 'Completed']);
        DB::table('ticket_statuses')->insert(['name' => 'Rejected']);

    }
}
