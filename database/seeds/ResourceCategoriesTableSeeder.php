<?php

use Illuminate\Database\Seeder;

class ResourceCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resource_categories')->insert(['name'=> 'Human Resource']);
        DB::table('resource_categories')->insert(['name'=> 'Materials']);
    }
}
