<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        	->insert([
        		'name' => 'John Admin',
        		'email' => 'admin@email.com',
        		'role_id' => 1,
        		'password' => sha1('admin1234')
        	]);

        DB::table('users')
        	->insert([
        		'name' => 'John Audit',
        		'email' => 'admin@email.com',
        		'role_id' => 2,
        		'password' => sha1('audit1234')
        	]);

        DB::table('users')
        	->insert([
        		'name' => 'John Manager',
        		'email' => 'manager@email.com',
        		'role_id' => 3,
        		'password' => sha1('user1234')
        	]);
        DB::table('users')
        	->insert([
        		'name' => 'John Employee',
        		'email' => 'employee@email.com',
        		'role_id' => 4,
        		'password' => sha1('user1234')
        	]);
    }
}
