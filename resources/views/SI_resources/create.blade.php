@extends('layouts.app')

@section('content')

	<section class="container">
		<div class="row">
			<div class="col-12 col-md-8 m-auto">
				@if (Session::has('update_failed'))
					<div class="alert alert-warning">
						{{Session::get('update_failed')}}
					</div>
				@endif
				@if ($errors->any() && !$errors->has('add-category'))
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">
							<small>{{$error}}</small>
						</div>
					@endforeach
				@endif

				<h1>{{isset($si_resource)? "Edit Resource" : "Add Resource"}}</h1>
				<form method="POST" enctype="multipart/form-data" action="
					@if (isset($si_resource))
						{{route('si_resource.update',['si_resource' => $si_resource->id])}}
					@else
						{{route('si_resource.store')}}
					@endif">

					@csrf
					@if (isset($si_resource))
						@method('PUT')
					@endif
					<div class="form-group">
						<label for="name">Name:</label>
						<input type="text" name="name" id="name" class="form-control" value="{{isset($si_resource)? $si_resource->name : ""}}"
						required="">
					</div>
					<div class="form-group">
						<label for="category">Category:</label>
						<select class="form-control" id="category" name="category">
							@foreach ($categories as $category)
								
								{{-- options --}}
								<option value="{{$category->id}}"
									@if (isset($si_resource))
										@if($category->id == $si_resource->id)
											selected
										@endif
									@endif	
								>{{$category->name}}</option>
								{{-- end of options --}}

							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label for="stocks">Stocks:</label>
						<input type="number" name="stocks" id="stocks" placeholder="input number of stocks" class="form-control" value="{{isset($si_resource)? $si_resource->stocks : ""}}">
					</div>

					<div class="form-group">
						<label for="details">Details:</label>
						<textarea name="details" id="details" class="form-control">{{
							isset($si_resource)? $si_resource->details : ""
						}}</textarea>
					</div>
					<div class="form-group">
						<label for="image">Image:</label>
						<input type="file" name="image" id="image" class="form-control-file">
					</div>
					<button class="btn btn-primary btn-block">{{!isset($si_resource)? "Add Resource": "Edit Resource"}}</button>
				</form>

			</div>
			<div class="col-12 col-md-3">
				@if (Session::has('category_message'))
					<div class="alert alert-success">
						<small>{{Session::get('category_message')}}</small>
					</div>
				@endif
				@if ($errors->has('add-category'))
					<div class="alert alert-danger">
						<small>Category Not Added</small>
					</div>
				@endif
				<form method="POST" action="{{route('resource_categories.store')}}">
					@csrf
					<div class="form-group mx-auto text-center">
						<h3>Add Category</h3>
						
						<input class="form-control" type="text" name="add-category" id="add-category" placeholder="Add Category">
						<button class="btn btn-secondary btn-block">Submit</button>
					</div>
				</form>
				
			</div>
		</div>
		
	</section>
@endsection