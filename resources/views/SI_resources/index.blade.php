@extends('layouts.app')

@section('content')
<section class="container">
	<div class="row">
		<div class="col-12 col-md-10 mx-auto">
					
			<div class="table-responsive">
				<h1 class="text-center">Resources
					@can('isAdmin')
						<a href="{{route('si_resource.create')}}" class="btn btn-outline-info">Add Resource</a>
					@endcan


				</h1>

				<table class="table table-hover table-striped table-primary rounded">
					<thead>
						<tr class="text-center">
							<th>Name</th>
							<th>Resource ID</th>
							<th>{{strtolower(Auth::user()->role->name) == 'employee'? 'Quantity' : 'Control Number'}}</th>
							<th>No. of Stocks</th>
							<th>Details</th>
							<th>
								<div class="form-check form-group">
									<input class="form-check-input" type="checkbox" name="selectAll" id="selectAll" onclick="toggle(this)"> 
								</div>
							</th>
						</tr>
					</thead>
					<tbody>
						<form id="checkboxForm" method="POST" action="{{route('temp.store')}}">
							@foreach ($si_resource as $resource)
							{{-- expr --}}
							@csrf
							<tr class="text-center">
								<th>{{$resource->name}}</th>
								<td>{{$resource->id}}</td>
								<td>
									@if (strtolower(Auth::user()->role->name) == 'employee')
										{{-- expr --}}
									<div class="form-group">
										<div class="input-group mb-3">
											{{-- <div class="input-group-prepend">
												<button class="btn btn-outline-secondary deduct-qty" type="button" data-id="{{$resource->id}}">-</button>
											</div> --}}
											<input type="text" class="form-control input-quantity" name="quantity{{$resource->id}}"value="1" min="1"  data-id="{{$resource->id}}">
											{{-- <div class="input-group-append">
												<button class="btn btn-outline-secondary add-qty" type="button"  data-id="{{$resource->id}}">+</button>
											</div> --}}
										</div>
									</div>
									@else
										{{$resource->control_number}}
									@endif
								</td>
								<td>{{$resource->stocks}}</td>
								<td>
									<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#a{{$resource->id}}">View Details</button>
								</td>
								<td>
									<input type="checkbox" name="{{$resource->id}}">
								</td>
								
							</tr>

							@endforeach
						</form>
					</tbody>
					<tfoot class="table-light">
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>

							<td>
								
							</td>
							
							    {{-- expr --}}
								<td>
									@cannot('isAdmin')
										<button class="btn btn-primary" type="submit" form="checkboxForm">Request</button>
									@endcannot
								</td>
						</tr>

					</tfoot>

				</table>

			</div>

		</div>
	</div>

</section>


@foreach ($si_resource as $resource)
<!-- Modal -->
<div class="modal fade" id="a{{$resource->id}}" tabindex="-1" role="dialog" aria-labelledby="label{{$resource->id}}" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="label{{$resource->id}}">Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<section class="modal-body">
				<div class="row">

					<div class="col-12 col-md-8 mx-auto">


						<div class="card">
							<img src="{{url('/public/'.$resource->images)}}" class="card-img-top img-fluid">
							<div class="card-body">

								<h2 class="card-title">{{$resource->name}}</h2>
								<p class="card-text">
									<strong>No. of Stocks: </strong>{{$resource->stocks}}
								</p>
								<p class="card-text">
									<strong>Details: </strong>{{$resource->details}}
								</p>
							</div>

							@can('isAdmin')
								<div class="card-footer">

									{{-- @can('isAdmin') --}}
									{{-- edit button --}}
									<a class="btn btn-info btn-block" href="{{route('si_resource.edit',['si_resource'=>$resource->id])}}">Edit Item</a>

									{{-- delete button --}}
									<form method="POST"class="my-2" action="{{route('si_resource.destroy',['si_resource' => $resource->id])}}">
										@csrf
										@method('DELETE')
										<button class="btn btn-warning btn-block">Delete Resource</button>
									</form>
									{{-- @endcan --}}
								</div>
							@endcan


						</div>

					</div>

				</div>

			</section>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				@cannot('isAdmin')
					<button class="btn btn-primary" type="submit" form="checkboxForm">Request</button>
				@endcannot
			</div>
		</div>
	</div>
</div> {{-- end modal --}}

@endforeach


@endsection