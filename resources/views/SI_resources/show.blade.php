@extends('layouts.app')

@section('content')

<section class="container">
	<div class="row">

		<div class="col-12 col-md-8 mx-auto">
			@if (Session::has('update_success'))
				<div class="alert alert-success">
					{{Session::get('update_success')}}
				</div>
			@endif	


			<div class="card">
				<img src="{{url('/public/'.$si_resource->images)}}" class="card-img-top">
				<div class="card-body">

					<h2 class="card-title">{{$si_resource->name}}</h2>
					<p class="card-text">
						<strong>No. of Stocks: </strong>{{$si_resource->stocks}}
					</p>
					<p class="card-text">
						<strong>Details: </strong>{{$si_resource->details}}
					</p>
				</div>
				<div class="card-footer">

					{{-- view button --}}
					<a href="{{route('si_resource.index')}}" class="btn btn-primary btn-block">View All Resource</a>

					{{-- @can('isAdmin') --}}
					{{-- edit button --}}
					<a class="btn btn-info btn-block" href="{{route('si_resource.edit',['si_resource'=>$si_resource->id])}}">Edit Item</a>

					{{-- delete button --}}
					<form method="POST"class="my-2" action="{{route('si_resource.destroy',['si_resource' => $si_resource->id])}}">
						@csrf
						@method('DELETE')
						<button class="btn btn-warning btn-block">Delete Resource</button>
					</form>
					{{-- @endcan --}}
				</div>


			</div>

		</div>

	</div>

</section>

@endsection