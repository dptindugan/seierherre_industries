@extends('layouts.app')

@section('content')

<section class="container">
	<div class="row">
		<div class="col-12 col-md-8 m-auto text-center">
			<div class="row">
				<div class="col-12 col-md-8">
					<h1>Resource Categories</h1>
				</div>
				<div class="col-12 col-md-4">
					<button class="btn btn-outline-info" data-toggle="modal" data-target="#addCategory" type="button" >Add Category</button>
				</div>
			</div>

			@if (count($resource_categories))
				

				@if (Session::has('update_failed'))
				<div class="alert alert-warning">
					<small>{{Session::get('update_failed')}}</small>
				</div>
				@endif
				@if (Session::has('delete_success'))
				<div class="alert alert-success">
					<small>{{Session::get('delete_success')}}</small>
				</div>
				@endif
				@if (Session::has('deleteAll_success'))
				<div class="alert alert-success">
					<small>{{Session::get('deleteAll_success')}}</small>
				</div>
				@endif
				@if (Session::has('update_success'))
				<div class="alert alert-success">
					<small>{{Session::get('update_success')}}</small>
				</div>
				@endif
				@if (Session::has('category_message'))
				<div class="alert alert-success">
					<small>{{Session::get('category_message')}}</small>
				</div>
				@endif
				@if ($errors->has('add-category'))
				<div class="alert alert-danger">
					<small>Category Not Added</small>
				</div>
				@endif
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th class="text-left">Category Names:</th>
								<th>
									<form method="POST" action="{{route('resource_categories.deleteAll')}}">
										@csrf
										@method('DELETE')
										<button class="btn-outline-danger btn btn-block">DELETE ALL</button>
									</form>
								</th>
								<th><button class="btn-danger btn btn-block">DELETE</button></th>
								<th class="text-right">
									<input type="checkbox" name="">
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($resource_categories as $category)
							{{-- expr --}}
							<tr>
								<td class="text-left">{{$category->name}}</td>
								<td><button class="btn btn-warning btn-block" data-toggle="modal" data-target="#a{{$category->id}}" type="button">Edit</button></td>
								<td>
									<form method="POST" action="{{route('resource_categories.destroy',['resource_category'=>$category->id])}}">
										@csrf
										@method('DELETE')
										<button class="btn-danger btn btn-block">DELETE</button>
									</form>
								</td>
								<td class="text-right">
									<input type="checkbox" name="">
								</td>
							</tr>

							@endforeach

						</tbody>

					</table>

				</div>
			@else
				@if (Session::has('update_failed'))
				<div class="alert alert-warning">
					<small>{{Session::get('update_failed')}}</small>
				</div>
				@endif
				@if (Session::has('delete_success'))
				<div class="alert alert-success">
					<small>{{Session::get('delete_success')}}</small>
				</div>
				@endif
				@if (Session::has('deleteAll_success'))
				<div class="alert alert-success">
					<small>{{Session::get('deleteAll_success')}}</small>
				</div>
				@endif
				@if (Session::has('update_success'))
				<div class="alert alert-success">
					<small>{{Session::get('update_success')}}</small>
				</div>
				@endif
				@if (Session::has('category_message'))
				<div class="alert alert-success">
					<small>{{Session::get('category_message')}}</small>
				</div>
				@endif
				@if ($errors->has('add-category'))
				<div class="alert alert-danger">
					<small>Category Not Added</small>
				</div>
				@endif
				<button class="btn btn-primary btn-block my-5" data-toggle="modal" data-target="#addCategory" type="button" >Add Category</button>
				<button></button>
			@endif
		</div>

	</div>

</section>

@foreach ($resource_categories as $resource_category)
<!-- Modal -->
<div class="modal fade" id="a{{$resource_category->id}}" tabindex="-1" role="dialog" aria-labelledby="label{{$resource_category->id}}" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="label{{$resource_category->id}}">Edit Category</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<section class="modal-body">
				<form id="categoryUpdateForm" method="POST"  action="{{route('resource_categories.update',['resource_category' => $resource_category->id])}}">
					<div class="row">
						<div class="col-12 col-md-8 mx-auto">

							@csrf
							@method('PUT')
							<div class="form-group mx-auto text-center">
								<h3>{{$resource_category->name}}</h3>

								<input class="form-control" type="text" name="categoryName" id="add-category" value="{{$resource_category->name}}" placeholder="Enter Name">

							</div>
							{{-- <button class="btn btn-primary btn-block">Update</button> --}}
						</div>

					</div>

				</section>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button  class="btn btn-primary">UPDATE</button>
				</div>
			</form>
		</div>
	</div>
</div> {{-- end modal --}}

@endforeach

{{-- add category modal --}}
<div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="labelAddCategory" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="labelAddCategory">Add Category</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<section class="modal-body">
				<div class="row">
					<div class="col-12 col-md-8 mx-auto">

						<form method="POST" id="addCategoryForm" action="{{route('resource_categories.store')}}">
							@csrf
							<div class="form-group mx-auto text-center">
								<h3>Add Category</h3>

								<input type="hidden" name="addCategoryModal" value="false">
								<input class="form-control" type="text" name="add-category" id="add-category2" placeholder="Add Category">
								<button class="btn btn-secondary btn-block">Add</button>

							</div>
						</form>
					</div>

				</div>

			</section>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" form="addCategoryForm" class="btn btn-primary">Add</button>
			</div>
		</div>
	</div>
</div> {{-- end modal --}}


@endsection