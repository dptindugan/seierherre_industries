<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta http-equiv="X-UA-Universal">
    <title>Seierherre Industries</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body class="landing py-5">
    @auth

        <ul class="top-right list-unstyled list-group list-group-horizontal">
            <li class="list-group-item bg-transparent border-0"><a href="{{ url('/home') }}" class="btn btn-link text-light text-decoration-none">Home</a></li>
            <li class="dropdown list-group-item bg-transparent border-0">
                <a id="ldropdown" class="dropdown-toggle btn btn-primary" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="ldropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>

    @endauth

    <section class="container border-info py-5 absolute">
        <h1 class="display-5">Seierherre Industries</h1>
        <div class="row mx-auto">
            <div class="col-12 col-md-6">
                <div class="row no-gutters landingSelect">

                    <div class="col-6 col-md-4 mb-2">
                        <a href="{{route('projects.index')}}" class="card btn dashboard selectButton">

                            <h1>View Assets</h1>
                            <h3 class="optionName h1styling">Dashboard</h3>


                        </a>
                    </div>
                    <div class="col-6 col-md-4 mb-2">
                        <a href="{{route('projects.index')}}" class="card btn project selectButton">
                            
                            <h1>View Assets</h1>
                            <h3 class="optionName h1styling">View Projects</h3>

                           
                        </a>
                    </div>
                    <div class="col-6 col-md-4 mb-2">
                        <a href="{{route('si_resource.index')}}" class="card btn resources selectButton">

                            <h1 class="card-title">View Assets</h1>
                            <h3 class="optionName h1styling">View Resources</h3>


                        </a>
                    </div>
                    <div class="col-6 col-md-4 mb-2">
                        <button class="card btn disabled users selectButton">

                            <h1>View Assets</h1>
                            <h3 class="optionName h1styling">Manage Users</h3>


                        </button>
                    </div>
                    <div class="col-6 col-md-4 mb-2">
                        <button class="card btn disabled create-note selectButton">

                            <h1>View Assets</h1>
                            <h3 class="optionName h1styling">Write a note</h3>


                        </button>
                    </div>
                    <div class="col-6 col-md-4 mb-2">
                        <button class="card btn disabled setting selectButton">
                            {{-- <img class="setting" src="{{ asset('images/settings.png') }}" alt=""> --}}
                            <h1>View Assets</h1>
                            <h3 class="optionName h1styling">Settings</h3>
                                

                        </button>
                    </div>
                    
                </div> {{-- end of row --}}

            </div> {{-- end of column --}}
            <div class="col-12 col-md-6">
                @auth
                <div class="offset-md-3 col-md-9">

                    <div class="card"> {{-- card --}}

                        <div class="card-body">  {{-- carousel --}}


                            <div id="carouselControl" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                <div class="carousel-item active">
                                  <img src="..." class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                  <img src="..." class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                  <img src="..." class="d-block w-100" alt="...">
                              </div>
                          </div>
                          <a class="carousel-control-prev" href="#carouselControl" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselControl" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>{{-- end of carousel --}}

                </div>  {{-- end of card --}}

            </div> 

        </div>

        @else
        <div class="row justify-content-center"> {{-- start of login form --}}
            <div class="offset-md-3 col-md-9">
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-8">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0 ">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Login') }}
                                    </button>

                                </div>
                                <div class="text-center">
                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif
                                    @if (Route::has('register'))
                                    <a class="btn btn-link" href="{{ route('register') }}">Register an account</a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> {{-- end of login form --}}
        @endauth

    </div>
</div> {{-- end of row --}}

</section>  

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>