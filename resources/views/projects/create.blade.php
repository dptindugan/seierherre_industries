@extends('layouts.app')

@section('content')
	
	<section class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h1>Start A project</h1>
				<form method="POST" action="{{route('projects.store')}}">
					@csrf
					<div class="form-group">
						
						<label class="form-control-label" for="name">Name:</label>
						<input class="form-control" type="text" name="name" placeholder="Project Name" id="name">		
						
					</div>
					<div class="form-group">
						<label class="form-control-label" for="details">Details</label>
						<textarea class="form-control" id="details" name="details" placeholder="Enter Details"></textarea>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="projectManager">Select Project Manager</label>
						<select class="form-control" id="projectManager" name="projectManager">
							@foreach ($managers as $manager)
								{{-- expr --}}
								<option value="{{$manager->id}}">{{$manager->name}}</option>
							@endforeach
						</select>
					</div>
					<button class="btn btn-primary btn-block">Submit</button>
				</form>				
			</div>
		</div>	
		
	</section>

@endsection