@extends('layouts.app')

@section('content')

<section class="container">
	<div class="row">

		<div class="col-12 col-md-8 mx-auto">
			<h2 class="text-center">Edit Project</h2>
			<form method="POST" id="updateForm" action="{{route('projects.update',['project' => $project->id])}}">
				@csrf
				@method('PUT')
				<div class="form-group">

					<label class="form-control-label" for="name">Name:</label>
					<input class="form-control" type="text" name="name" placeholder="Project Name" id="name" value="{{$project->name}}">		

				</div>
				<div class="form-group">
					<label class="form-control-label" for="details">Details</label>
					<textarea class="form-control" id="details" name="details" placeholder="Enter Details">{{$project->details}}</textarea>
				</div>
				<div class="form-group">
					<label class="form-control-label" for="projectManager">Select Project Manager</label>
					<select class="form-control" id="projectManager" name="projectManager">
						@foreach ($users as $user)

						<option value="{{$user->id}}"
							{{$user->id == $project->project_manager_id? 'selected' : ''}}
							>{{$user->name}}</option>
						}
						@endforeach
					</select>
				</div>

				<button class="btn btn-primary btn-block">Update</button>

			</form>

		</div>
	</section>


	@endsection