@extends('layouts.app')


@section('content')

	<section class="container">
		<div class="row">
			<div class="col-10 m-auto">
				<h1 class="d-inline-block">Projects</h1>
				@can('isAdmin')
					<a class="btn btn-link text-right" href="{{ route('projects.create') }}">create</a>
				@endcan
				<div class="table-responsive">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Project Name:</th>
								<th>Project Code:</th>
								<th>Project Manager:</th>
								<th>Project Status:</th>
								<th>Project Details</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($projects as $project)
								{{-- expr --}}
								<tr>
									<td>{{$project->name}}</td>
									<td>{{$project->project_code}}</td>
									<td>{{$project->projectManager->name}}</td>
									<td>{{$project->projectStatus->name}}</td>
									<td><button class="btn btn-primary btn-block" data-toggle="modal" data-target="#descModal">View Details</button></td>
								</tr>
							@endforeach
						</tbody>
						
					</table>
					
				</div>
				
			</div>
			
		</div>
		
	</section>

	@foreach ($projects as $project)

		<!-- Modal -->
		<div class="modal fade" id="descModal" tabindex="-1" role="dialog" aria-labelledby="descModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="descModalLabel">Details</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<section class="modal-body">
						<div class="row">

							<div class="col-12 col-md-8 mx-auto">

								<p>{{$project->details}}</p>

								@can('isAdmin')
								    {{-- expr --}}
									<a href="{{route('projects.edit',['project' => $project->id])}}" class="btn btn-warning btn-block my-2">Edit</a>

									<form method="POST" action="{{route('projects.destroy',['project' => $project->id])}}">
										
										@csrf
										@method('DELETE')
										<button class="btn btn-danger btn-block">Delete</button>

									</form>

								@endcan
								
							</div>

						</div>

					</section>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div> {{-- end modal --}}
	@endforeach
	
@endsection