@extends('layouts.app')

@section('content')

							{{-- {{dd($project->manager)}} --}}
{{-- {{dd($project->manager->name)}} --}}

<section class="container">
	<div class="row">
		<div class="col-12 col-md-8 m-auto">

			<div class="card">

				<div class="card-body">

					<h2 class="card-title">{{$project->name}}</h2>
					<p class="card-text">
						<strong>Project Code: </strong>{{$project->project_code}}
					</p>
					<p class="card-text">
						<strong>Project Manager: </strong>
						@foreach ($users as $user)
							@if ( $user->id == $project->project_manager_id)
								{{$user->name}}
							@endif
						@endforeach
					</p>
					<p class="card-text">
						<strong>Details: </strong>{{$project->details}}
					</p>
					<p class="card-text">
						<strong>Status: </strong><span class="badge badge-info">{{$project->projectStatus->name}}</span>
					</p>
				</div>
				<div class="card-footer">

					{{-- view button --}}
					<a href="{{route('projects.index')}}" class="btn btn-primary btn-block">View All Projects</a>

					{{-- @can('isAdmin') --}}
					{{-- edit button --}}
					{{-- <a class="btn btn-info btn-block" href="{{route('projects.edit',['project'=> $project->id])}}">Edit Project</a> --}}
					<button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#updateModal">Edit Project</button>

					{{-- delete button --}}
					<form method="POST"class="my-2" action="{{route('projects.destroy',['project' => $project->id])}}">
						@csrf
						@method('DELETE')
						<button class="btn btn-warning btn-block">Delete Project</button>
					</form>
					{{-- @endcan --}}
				</div>


			</div>

		</div>
	</div>

</section>
<!-- Modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="updateLabel">Edit Project</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<section class="modal-body">
				<div class="row">

					<div class="col-12 col-md-8 mx-auto">
						<h2>{{$project->name}}</h2>
						<form method="POST" id="updateForm" action="{{route('projects.update',['project' => $project->id])}}">
							@csrf
							@method('PUT')
							<div class="form-group">

								<label class="form-control-label" for="name">Name:</label>
								<input class="form-control" type="text" name="name" placeholder="Project Name" id="name" value="{{$project->name}}">		

							</div>
							<div class="form-group">
								<label class="form-control-label" for="details">Details</label>
								<textarea class="form-control" id="details" name="details" placeholder="Enter Details">{{$project->details}}</textarea>
							</div>
							<div class="form-group">
								<label class="form-control-label" for="projectManager">Select Project Manager</label>
								<select class="form-control" id="projectManager" name="projectManager">
									@foreach ($users as $user)

										<option value="{{$user->id}}"
											{{$user->id == $project->project_manager_id? 'selected' : ''}}
											>{{$user->name}}</option>
											}
									@endforeach
								</select>
							</div>

						</form>
						
					</div>

				</div>

			</section>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" form="updateForm" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</div>
</div> {{-- end modal --}}



@endsection