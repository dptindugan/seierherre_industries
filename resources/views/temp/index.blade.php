@extends('layouts.app')

{{-- {{dd($si_resources)}} --}}
@section('content')
<section class="container">
	<div class="row">
		<div class="col-12 col-md-10 mx-auto">
			<h1 class="text-center">Requested Resources</h1>
			@if (Session::has('temp') && count(Session::get('temp')))
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>

						<tr>
							<th>Name</th>
							<th>Resource ID</th>
							<th>Control Number</th>
							<th>Quantity</th>
							<th colspan="3">Details</th>
							{{-- <th></th> --}}
						</tr>

					</thead>
					<tbody>

						{{-- expr --}}
						@foreach ($si_resources as $resource)
						{{-- expr --}}

						<tr>
							<td>{{$resource->name}}</td>
							<td>{{$resource->id}}</td>
							<td>{{$resource->control_number}}</td>
							<td>
								<form method="POST" id="updateForm{{$resource->id}}" action="{{route('temp.update', ['temp'=>$resource->id])}}">
									@csrf
									@method('PUT')
									<input type="hidden" name="id" value="{{$resource->id}}">
									<div class="form-group">
										<div class="input-group">

											<input type="number" name="quantity" data-id="{{$resource->id}}"value="{{$resource->quantity}}" class="form-control">
											<div class="input-group-append">
												<button class="btn btn-secondary" type="submit" form="updateForm{{$resource->id}}" data-id="{{$resource->id}}">Update</button>
											</div>
										</div>
									</div>
								</form>
							</td>
							<td><button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#a{{$resource->id}}">View Details</button></td>
							<td>
								<form action="{{route('temp.destroy',[ 'temp' => $resource->id])}}" method="POST">
									@csrf
									@method('DELETE')
									<button class="btn btn-outline-danger">Cancel</button>
								</form>
							</td>
						</tr>

						@endforeach

					</tbody>

					<tfoot>
						<tr>
							<th>
								@can('isManager')
									Select Project
								@endcan
							</th>
							<form method="POST" action="{{route('tickets.store')}}">
								<td colspan="2">
									@can('isManager')

										<select class="form-control" name="project">
											@foreach ($projects as $project)
											<option value="{{$project->id}}">{{$project->name}}</option>
											@endforeach
										</select>

									@endcan
									@can('isEmployee')
										<input type="hidden" name="project" value="{{ Auth::user()->project_id }}">
									@endcan
								</td>
								<td></td>
								<td>
									@csrf
									<button class="btn btn-success">Confirm Request</button>
								</td>
							</form>
							<td>
								<form  action="{{route('temp.clear')}}" method="POST">
									@csrf
									@method('DELETE')
									<button class="btn btn-danger btn-block">Cancel Requests</button>
								</form>
							</td>

						</tr>
					</tfoot>


				</table>

			</div>

		</div>

	</div>


</section>


@foreach ($si_resources as $resource)
<!-- Modal -->
<div class="modal fade" id="a{{$resource->id}}" tabindex="-1" role="dialog" aria-labelledby="label{{$resource->id}}" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="label{{$resource->id}}">{{$resource->name}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<section class="modal-body">
				<div class="row">

					<div class="col-12 col-md-8 mx-auto">


						<div class="card">
							<img src="{{url('/public/'.$resource->images)}}" class="card-img-top img-fluid">
							<div class="card-body">

								<h2 class="card-title">{{$resource->name}}</h2>
								<p class="card-text">
									<strong>No. of Stocks: </strong>{{$resource->stocks}}
								</p>
								<p class="card-text">
									<strong>Details: </strong>{{$resource->details}}
								</p>
							</div>
							<div class="card-footer">

								{{-- @can('isAdmin') --}}
								{{-- edit button --}}

								{{-- delete button --}}
								{{-- <form method="POST"class="my-2" action="{{route('si_resource.destroy',['si_resource' => $resource->id])}}">
									@csrf
									@method('DELETE')
									<button class="btn btn-warning btn-block">Delete Resource</button>
								</form> --}}
								{{-- @endcan --}}
							</div>


						</div>

					</div>

				</div>

			</section>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Request</button>
			</div>
		</div>
	</div>
</div> {{-- end modal --}}

@endforeach

@else
	<div class="alert alert-warning">
		<h3 class="text-center">You have no request</h3>
	</div>
	<a href="{{route('si_resource.index')}}" class="btn btn-info btn-block">Request a Resource</a>

@endif

@endsection