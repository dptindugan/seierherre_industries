@extends('layouts.app')

@section('content')
<div class="container"> 
	<div class="row">
		<div class="col-12 col-md-10 mx-auto">
			{{-- <h2 class="display-5 text-center">Requests</h1> --}}
			@if (Session::has('update_error'))
				<span class="alert alert-danger">{{ Session::get('update_error') }}</span>
				
			@endif
			<nav>
				<div class="nav nav-tabs" id="nav-tab" role="tablist">

					@cannot('isAudit')
					<a class="nav-item nav-link active" id="nav-index-tab" data-toggle="tab" href="#nav-index" role="tab" aria-controls="nav-index" aria-selected="true">All Requests</a>

					@cannot('isAdmin')
					<a class="nav-item nav-link" id="nav-pending-tab" data-toggle="tab" href="#nav-pending" role="tab" aria-controls="nav-pending" aria-selected="false">Pending</a>
					@endcannot

					<a class="nav-item nav-link" id="nav-processed-tab" data-toggle="tab" href="#nav-processed" role="tab" aria-controls="nav-processed" aria-selected="false">{{ strtolower(Auth::user()->role->name) == 'admin' ? 'Pending': 'Processed'}}</a>

					<a class="nav-item nav-link" id="nav-approved-tab" data-toggle="tab" href="#nav-approved" role="tab" aria-controls="nav-approved" aria-selected="false">Approved</a>

					<a class="nav-item nav-link" id="nav-completed-tab" data-toggle="tab" href="#nav-completed" role="tab" aria-controls="nav-completed" aria-selected="false">Completed</a>

					<a class="nav-item nav-link" id="nav-rejected-tab" data-toggle="tab" href="#nav-rejected" role="tab" aria-controls="nav-rejected" aria-selected="false">Rejected</a>

					@endcannot

					@can('isAudit')



					<a class="nav-item nav-link active" id="nav-index-tab" data-toggle="tab" href="#nav-index" role="tab" aria-controls="nav-index" aria-selected="true">All Requests</a>

					<a class="nav-item nav-link" id="nav-approved-tab" data-toggle="tab" href="#nav-approved" role="tab" aria-controls="nav-approved" aria-selected="false">Approved</a>
					
					<a class="nav-item nav-link" id="nav-completed-tab" data-toggle="tab" href="#nav-completed" role="tab" aria-controls="nav-completed" aria-selected="false">Completed</a>

					@endcan


				</div>
			</nav>
			<div class="tab-content" id="nav-tabContent">

				{{-- request index --}}
				<div class="tab-pane fade show active" id="nav-index" role="tabpanel" aria-labelledby="nav-index-tab">
					@if ($tickets)
						{{-- expr --}}
					@endif
					@foreach ($tickets as $ticket)
					<div class="accordion" id="accordionExample">
						<div class="card">
							<div class="card-header" id="headingOne">
								<h2 class="mb-0">
									<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#ticket-{{$ticket->id}}" aria-expanded="true" aria-controls="collapseOne">
										{{strtoupper($ticket->reference_no)}}/{{($ticket->created_at)->format('F d, Y - h:i:s')}}
										@if (Session::has('updated_ticket') && Session::get('updated_ticket') == $ticket->id)
										<span class="badge badge-info">
											Status Updated
										</span>
										{{-- expr --}}
										@endif
										@if (strtolower($ticket->ticketStatus->name) == "pending")
										{{-- expr --}}
										<span class="badge badge-warning float-right">{{$ticket->ticketStatus->name}}</span>

										@elseif (strtolower($ticket->ticketStatus->name) == "processed")
										@cannot('isAdmin')
										<span class="badge badge-info float-right">{{$ticket->ticketStatus->name}}</span>
										@endcannot

										@can('isAdmin')
										<span class="badge badge-warning float-right">Pending</span>
										@endcan



										@elseif(strtolower($ticket->ticketStatus->name) == "approved")
										<span class="badge badge-success float-right">{{$ticket->ticketStatus->name}}</span>


										@elseif(strtolower($ticket->ticketStatus->name) == "completed")

										<span class="badge badge-primary float-right">{{$ticket->ticketStatus->name}}</span>


										@elseif(strtolower($ticket->ticketStatus->name) == "rejected")

										<span class="badge badge-danger float-right">{{$ticket->ticketStatus->name}}</span>


										@endif
									</button>
								</h2>
							</div>

							<div id="ticket-{{$ticket->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body">
									{{-- start of card body --}}
									<h5 class="card-title">Request Summary</h5>
									<div class="table-responsive mb-3">
										<table class="table table-sm table-bordered">
											<tbody>
												<tr>
													<td>Employee Name:</td>
													<td><strong>{{$ticket->user->name}}</strong></td>
												</tr>
												<tr>
													<td>Reference Number:</td>
													<td><strong>{{strtoupper($ticket->reference_no)}}</strong></td>
												</tr>
												<tr>
													<td>Project Code:</td>
													<td>{{$ticket->project->project_code}}</td>
												</tr>
												@foreach ($ticket->resources as $ticket_resource)
												<tr>
													<td><strong>Resource: </strong>{{ $ticket_resource->name }}</td>
													<td><strong>Quantity: </strong>{{ $ticket_resource->pivot->quantity }}</td>
												</tr>
												@endforeach

												<tr>
													<td>Request Date</td>
													<td>{{$ticket->created_at->format('F d, Y')}}</td>
												</tr>

											</tbody>

											<tfoot>
												<tr>
													<td colspan="2">

														<a href="{{route('tickets.show',['ticket'=>$ticket->id])}}" class="btn btn-primary btn-block">View Details</a>


													</td>
												</tr>

												@cannot('isEmployee')
												@if ($ticket->ticket_status_id == 2)
													@can('isAdmin')
													<tr>
														<td colspan="2">	
															<form method="POST" action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
																	@csrf
																	@method('PUT')

					   												<input type="hidden" name="action_btn" value="approved">

																	<button class="btn btn-success btn-block">Approve</button>
															</form>
																
														</td>
													</tr>

													<tr>	
														<td colspan="2">
																
															<form method="POST" action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
																	@csrf
																	@method('PUT')

					   												<input type="hidden" name="action_btn" value="rejected">

																	<button href="{{route('tickets.update',['ticket'=>$ticket->id])}}" class="btn btn-danger btn-block">Reject</button>

															</form>

														</td>
													</tr>
													@endcan
												@endif
												@endcannot

												@can('isManager')
												@if ($ticket->ticket_status_id == 1)
													<tr>
														<td colspan="2">	
															<form method="POST" action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
																	@csrf
																	@method('PUT')

					   												<input type="hidden" name="action_btn" value="approved">

																	<button class="btn btn-success btn-block">Approve</button>
															</form>
																
														</td>
													</tr>

													<tr>	
														<td colspan="2">
																
															<form method="POST" action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
																	@csrf
																	@method('PUT')

					   												<input type="hidden" name="action_btn" value="rejected">

																	<button href="{{route('tickets.update',['ticket'=>$ticket->id])}}" class="btn btn-danger btn-block">Reject</button>

															</form>

														</td>
													</tr>
												@endif
												@endcan

												@can('isAudit')
												@if ($ticket->ticket_status_id == 3)
													<tr>
														<td colspan="2">
															<form method="POST" action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
																	@csrf
																	@method('PUT')

					   												<input type="hidden" name="action_btn" value="completed">

																	<button href="{{route('tickets.update',['ticket'=>$ticket->id])}}" class="btn btn-outline-info btn-block">Mark as Complete</button>

															</form>
														</td>
													</tr>
												@endif
												@endcan
											</tfoot>
										</table>

									</div>
								</div>
							</div>
						</div>

					</div>
						{{-- end of accordion --}}
					@endforeach


				</div> {{-- end request index --}}

				{{-- request pending --}}
				<div class="tab-pane fade" id="nav-pending" role="tabpanel" aria-labelledby="nav-pending-tab">

					@foreach ($tickets as $ticket)
					@if (strtolower($ticket->ticketStatus->name) == 'pending')
						<div class="accordion" id="accordionExample">

							<div class="card">

								<div class="card-header" id="headingOne">

									<h2 class="mb-0">

										<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#ticket-{{$ticket->id}}" aria-expanded="true" aria-controls="collapseOne">
											{{strtoupper($ticket->reference_no)}}/{{($ticket->created_at)->format('F d, Y - h:i:s')}}
											@if (Session::has('updated_ticket') && Session::get('updated_ticket') == $ticket->id)
											<span class="badge badge-info">
												Status Updated
											</span>
											{{-- expr --}}
											@endif
											

											<span class="badge badge-warning float-right">{{$ticket->ticketStatus->name}}</span>


										</button>
									</h2>
								</div>

								<div id="ticket-{{$ticket->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
									<div class="card-body">
										{{-- start of card body --}}
										<h5 class="card-title">Request Summary</h5>
										<div class="table-responsive mb-3">
											<table class="table table-sm table-bordered">
												<tbody>
													<tr>
														<td>Employee Name:</td>
														<td><strong>{{$ticket->user->name}}</strong></td>
													</tr>
													<tr>
														<td>Reference Number:</td>
														<td><strong>{{strtoupper($ticket->reference_no)}}</strong></td>
													</tr>

													<tr>
														<td>Project name:</td>
														<td>{{$ticket->project->name}}</td>
													</tr>

													<tr>

													@foreach ($ticket->resources as $ticket_resource)
													<tr>
														<td><strong>Resource: </strong>{{ $ticket_resource->name }}</td>
														<td><strong>Quantity: </strong>{{ $ticket_resource->pivot->quantity }}</td>
														</tr>
													@endforeach

													</tr>

													<tr>

														<td>Request Date</td>
														<td>{{$ticket->created_at->format('F d, Y')}}</td>

													</tr>

												</tbody>
													
												<tfoot>

													<tr>

														<td colspan="2">

															<a href="{{route('tickets.show',['ticket'=>$ticket->id])}}" class="btn btn-primary btn-block">View Details</a>


														</td>

													</tr>

													@cannot('isEmployee')
													<tr>
														<td colspan="2">

															<form method="POST" action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
																@csrf
																@method('PUT')

																<button class="btn btn-success btn-block">Approve</button>
															</form>

														</td>
													</tr>

													<tr>
														<td colspan="2">

															<form method="POST" action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
																	@csrf
																	@method('PUT')

																	<button href="{{route('tickets.update',['ticket'=>$ticket->id])}}" class="btn btn-danger btn-block">Reject</button>

															</form>

														</td>
													</tr>
													@endcannot

												</tfoot>
											</table>

										</div>
									</div>
								</div>
							</div>

						</div>
						{{-- end of accordion --}}
					@endif	
					@endforeach

				</div> {{--end request pending --}}

				{{-- request approved --}}
				<div class="tab-pane fade" id="nav-approved" role="tabpanel" aria-labelledby="nav-approved-tab">

					@foreach ($tickets as $ticket)
					@if (strtolower($ticket->ticketStatus->name) == 'approved')
					<div class="accordion" id="accordionExample">
						<div class="card">
							<div class="card-header" id="headingOne">
								<h2 class="mb-0">
									<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#ticket-{{$ticket->id}}" aria-expanded="true" aria-controls="collapseOne">
												{{strtoupper($ticket->reference_no)}}/{{($ticket->created_at)->format('F d, Y - h:i:s')}}
										@if (Session::has('updated_ticket') && Session::get('updated_ticket') == $ticket->id)
										<span class="badge badge-info">
											Status Updated
										</span>
												
										@endif

										<span class="badge badge-success float-right">{{$ticket->ticketStatus->name}}</span>
										</button>

								</h2>

							</div>

							<div id="ticket-{{$ticket->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body">
											{{-- start of card body --}}
									<h5 class="card-title">Request Summary</h5>
									<div class="table-responsive mb-3">
										<table class="table table-sm table-bordered">
											<tbody>
												<tr>
													<td>Employee Name:</td>
													<td><strong>{{$ticket->user->name}}</strong></td>
												</tr>

												<tr>

													<td>Reference Number:</td>
													<td><strong>{{strtoupper($ticket->reference_no)}}</strong></td>

												</tr>

												<tr>

													<td>Project Code:</td>
													<td>{{$ticket->project->project_code}}</td>

												</tr>

												<tr>

												@foreach ($ticket->resources as $ticket_resource)
												<tr>
													<td><strong>Resource: </strong>{{ $ticket_resource->name }}</td>
													<td><strong>Quantity: </strong>{{ $ticket_resource->pivot->quantity }}</td>
												</tr>
												@endforeach


												</tr>

												<tr>

													<td>Date</td>
													<td>{{$ticket->created_at->format('F d, Y')}}</td>

												</tr>

											</tbody>
											
											<tfoot>
												<tr>

													<td colspan="2">

														<a href="{{route('tickets.show',['ticket'=>$ticket->id])}}" class="btn btn-primary btn-block">View Details</a>


													</td>	

												</tr>
												

											</tfoot>

										</table>

									</div>
								</div>
							</div>
						</div>

					</div>
					{{-- end of accordion --}}
					@endif	
					@endforeach



				</div> {{--end request approved --}}


				{{-- request rejected --}}
				<div class="tab-pane fade" id="nav-rejected" role="tabpanel" aria-labelledby="nav-rejected-tab">

					@foreach ($tickets as $ticket)
					@if (strtolower($ticket->ticketStatus->name) == 'rejected')

					<div class="accordion" id="accordionExample">

						<div class="card">

							<div class="card-header" id="headingOne">

								<h2 class="mb-0">

									<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#ticket-{{$ticket->id}}" aria-expanded="true" aria-controls="collapseOne">
										{{strtoupper($ticket->reference_no)}}/{{($ticket->created_at)->format('F d, Y - h:i:s')}}
										@if (Session::has('updated_ticket') && Session::get('updated_ticket') == $ticket->id)
										<span class="badge badge-info">
											Status Updated
										</span>
												{{-- expr --}}
										@endif

										<span class="badge badge-danger float-right">{{$ticket->ticketStatus->name}}</span>


									</button>

								</h2>

							</div>

							<div id="ticket-{{$ticket->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">

								<div class="card-body">
											{{-- start of card body --}}
									<h5 class="card-title">Request Summary</h5>

									<div class="table-responsive mb-3">

										<table class="table table-sm table-bordered">

											<tbody>

												<tr>
													<td>Employee Name:</td>
													<td><strong>{{$ticket->user->name}}</strong></td>
												</tr>

												<tr>

													<td>Reference Number:</td>
													<td><strong>{{strtoupper($ticket->reference_no)}}</strong></td>

												</tr>

												<tr>

													<td>Project Code:</td>
													<td>{{$ticket->project->project_code}}</td>

												</tr>

												<tr>

													@foreach ($ticket->resources as $ticket_resource)
													<tr>
														<td><strong>Resource: </strong>{{ $ticket_resource->name }}</td>
														<td><strong>Quantity: </strong>{{ $ticket_resource->pivot->quantity }}</td>
													</tr>
													@endforeach

												</tr>

												<tr>
													<td>Request Date</td>
													<td>{{$ticket->created_at->format('F d, Y')}}</td>
												</tr>

											</tbody>

											<tfoot>

												<tr>

													<td colspan="2">

														<a href="{{route('tickets.show',['ticket'=>$ticket->id])}}" class="btn btn-primary btn-block">View Details</a>


													</td>
												</tr>

											</tfoot>
										</table>

									</div>

								</div>

							</div>

						</div>

					</div>
								{{-- end of accordion --}}
					@endif	
					@endforeach

				</div> {{--end request rejected --}}


				{{-- request completed --}}
				<div class="tab-pane fade" id="nav-completed" role="tabpanel" aria-labelledby="nav-completed-tab">

					@foreach ($tickets as $ticket)
					@if (strtolower($ticket->ticketStatus->name) == 'completed')

					<div class="accordion" id="accordionExample">
						<div class="card">
							<div class="card-header" id="headingOne">
								<h2 class="mb-0">
									<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#ticket-{{$ticket->id}}" aria-expanded="true" aria-controls="collapseOne">
										{{strtoupper($ticket->reference_no)}}/{{($ticket->created_at)->format('F d, Y - h:i:s')}}
										@if (Session::has('updated_ticket') && Session::get('updated_ticket') == $ticket->id)
										<span class="badge badge-info">
											Status Updated
										</span>
												{{-- expr --}}
										@endif
												
										<span class="badge badge-primary float-right">{{$ticket->ticketStatus->name}}</span>

									</button>
								</h2>
							</div>

							<div id="ticket-{{$ticket->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">

								<div class="card-body">
											{{-- start of card body --}}
									<h5 class="card-title">Request Summary</h5>
									<div class="table-responsive mb-3">
										<table class="table table-sm table-bordered">
											<tbody>

												<tr>
													<td>Employee Name:</td>
													<td><strong>{{$ticket->user->name}}</strong></td>
												</tr>

												<tr>
													<td>Reference Number:</td>
													<td><strong>{{strtoupper($ticket->reference_no)}}</strong></td>
												</tr>

												<tr>
													<td>Project Code:</td>
													<td>{{$ticket->project->project_code}}</td>
												</tr>

												<tr>

													@foreach ($ticket->resources as $ticket_resource)
													<tr>
														<td><strong>Resource: </strong>{{ $ticket_resource->name }}</td>
														<td><strong>Quantity: </strong>{{ $ticket_resource->pivot->quantity }}</td>
													</tr>
													@endforeach
														
												</tr>

												<tr>

													<td>Date Completed</td>
													<td>{{$ticket->updated_at->format('F d, Y - h:i:s')}}</td>

												</tr>

											</tbody>

											<tfoot>
														
												<tr>

													<td colspan="2">

														<a href="{{route('tickets.show',['ticket'=>$ticket->id])}}" class="btn btn-primary btn-block">View Details</a>


													</td>

												</tr>

											</tfoot>

										</table>

									</div>

								</div>

							</div>

						</div>

					</div>
								{{-- end of accordion --}}
					@endif	
					@endforeach

				</div> {{--end request completed --}}

				{{-- request processed --}}
				<div class="tab-pane fade" id="nav-processed" role="tabpanel" aria-labelledby="nav-processed-tab">

					@foreach ($tickets as $ticket)
					@if (strtolower($ticket->ticketStatus->name) == 'processed')

					<div class="accordion" id="accordionExample">

						<div class="card">

							<div class="card-header" id="headingOne">

								<h2 class="mb-0">
									<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#ticket-{{$ticket->id}}" aria-expanded="true" aria-controls="collapseOne">
										{{strtoupper($ticket->reference_no)}}/{{($ticket->created_at)->format('F d, Y - h:i:s')}}
										@if (Session::has('updated_ticket') && Session::get('updated_ticket') == $ticket->id)
										<span class="badge badge-info">
											Status Updated
										</span>
										
										@endif

										@cannot('isAdmin')
										<span class="badge badge-info float-right">processed</span>
										@else
										<span class="badge badge-warning float-right">pending</span>

										@endcannot
										

									</button>

								</h2>
							</div>

							<div id="ticket-{{$ticket->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body">
									{{-- start of card body --}}
									<h5 class="card-title text-center">Request Summary</h5>
									<div class="table-responsive mb-3">

										<table class="table table-sm table-bordered">

											<tbody>

												<tr>
													<td>Employee Name:</td>
													<td><strong>{{$ticket->user->name}}</strong></td>
												</tr>

												<tr>
													<td>Reference Number:</td>
													<td><strong>{{strtoupper($ticket->reference_no)}}</strong></td>
												</tr>

												<tr>
													<td>Project Code:</td>
													<td>{{$ticket->project->project_code}}</td>
												</tr>

												<tr>
													
													@foreach ($ticket->resources as $ticket_resource)
													<tr>
														<td><strong>Resource: </strong>{{ $ticket_resource->name }}</td>
														<td><strong>Quantity: </strong>{{ $ticket_resource->pivot->quantity }}</td>
													</tr>
													@endforeach

												</tr>

												<tr>

													<td>Date</td>
													<td>{{$ticket->created_at->format('F d, Y')}}</td>
													
												</tr>

											</tbody>
												
											<tfoot>
													
												<tr>
													<td colspan="2">

														<a href="{{route('tickets.show',['ticket'=>$ticket->id])}}" class="btn btn-primary btn-block">View Details</a>


													</td>
												</tr>
												
												@can('isAdmin')

												<tr>
													<td colspan="2">

														<form method="POST" action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
																@csrf
																@method('PUT')

																<button class="btn btn-success btn-block">Approve</button>
														</form>

													</td>

												</tr>

												<tr>

													<td colspan="2">

														<form method="POST" action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
															@csrf
															@method('PUT')

															<button href="{{route('tickets.update',['ticket'=>$ticket->id])}}" class="btn btn-danger btn-block">Reject</button>

														</form>

													</td>

												</tr>

												@endcan

											</tfoot>

										</table>

									</div>
								</div>
							</div>
						</div>

					</div>
						{{-- end of accordion --}}
					@endif	
					@endforeach


				</div> {{--end request processed --}}

			</div>

		</div>
	</div>
</div>

@endsection