@extends('layouts.app')

{{-- {{ dd($ticket->project) }} --}}
@section('content')
	
	<div class="container">

	<div class="row">

		<div class="col-12 col-md-8 mx-auto">

			<h3 class="text-center">Request Details</h3>
			<hr class="jumbotron-hr">

		</div>


	</div>		
	<div class="row">

		<div class="col-12 col-md-8	mx-auto">
			<div class="table-responsive">
				<table class="table table-sm table-borderless">
					<div class="approve-btn-show-request">
						<h6 class="text-center text-muted">Action</h6>

						@cannot('isEmployee')
						   	@if ($ticket->ticket_status_id == 1)
						   	@can('isManager')
							<div class="my-2">
								
								<form method="POST" action="{{route('tickets.update',['ticket' => $ticket->id])}}">
									@csrf
									@method('PUT')

									<input type="hidden" name="action_btn" value="approved">
									<button class="btn btn-success btn-block">Approve Request</button>
									
								</form>
							</div>

							<div class="my-2"> 
								
								<form method="POST" action="{{route('tickets.update',['ticket' => $ticket->id])}}">
									
									@csrf
									@method('PUT')

									<input type="hidden" name="action_btn" value="rejected">
									<button class="btn btn-danger btn-block">Reject Request</button>
									
								</form>

							</div>

							@endcan
						   	@endif


					   	   	@if ($ticket->ticket_status_id == 2)
					   	   	@can('isAdmin')
					   		<div class="my-2">
					   			
					   			<form method="POST" action="{{route('tickets.update',['ticket' => $ticket->id])}}">
					   				@csrf
					   				@method('PUT')

					   				<input type="hidden" name="action_btn" value="approved">
					   				<button class="btn btn-success btn-block">Approve Request</button>
					   				
					   			</form>
					   		</div>

					   		<div class="my-2"> 
					   			
					   			<form method="POST" action="{{route('tickets.update',['ticket' => $ticket->id])}}">
					   				
					   				@csrf
					   				@method('PUT')

					   				<input type="hidden" name="action_btn" value="rejected">
					   				<button class="btn btn-danger btn-block">Reject Request</button>
					   				
					   			</form>

					   		</div>

					   		@endcan
					   	   	@endif
						@endcan

						<a class="btn btn-link text-center btn-block" href="{{ route('tickets.index') }}">View Requests</a>

					</div>
					
					<tbody>
						<tr>
							<td>Name:</td>
							<td><strong>{{$ticket->user->name}}</strong></td>
							

						</tr>
						<tr>
							<td>Project Name</td>
							<td><strong>{{$ticket->project->name}}</strong></td>
						</tr>
						<tr>
							<td>Reference No:</td>
							<td><strong>{{strtoupper($ticket->reference_no)}}</strong></td>
						</tr>
						<tr>
							<td>Status</td>
							<td>{{$ticket->ticketStatus->name}}</td>
						</tr>
						<tr>
							<td>Date</td>
							<td>{{$ticket->created_at->format('F d, Y')}}</td>
						</tr>
					</tbody>

				</table>
				<table class="table">
					<thead>
						<th scope="col">Resource Name</th>
						<th scope="col">Control No.</th>
						<th scope="col">Quantity</th>
						<th scope="col">Resource Details</th>
						{{-- <th scope="col">Action</th> --}}
					</thead>
					<tbody>
						@foreach ($ticket->resources as $ticket_resource)
							
							<tr>
								<td>{{$ticket_resource->name}}</td>
								<td>{{$ticket_resource->control_number}}</td>
								<td>{{$ticket_resource->pivot->quantity}}</td>
								<td>
									<div>

										<a class="btn btn-block btn-link text-left" data-toggle="collapse" href="#collapse{{$ticket_resource->id}}" role="button" aria-expanded="false" aria-controls="collapse{{$ticket_resource->id}}">
										  View Details
										</a>

										<div class="collapse" id="collapse{{$ticket_resource->id}}">
											<p>{{$ticket_resource->details}}</p>
										</div>

									</div>
								</td>
								{{-- <td>
									<form action="{{route('tickets.update',['ticket'=> $ticket->id])}}" method="POST">
										@csrf
										@method('PUT')
										
										
										<input type="hidden" name="resource_id" value="{{$ticket_resource->id}}">
										<input type="hidden" name="actionBtn" value="approved">
										<button class="btn btn-success btn-block mb-2">Approve</button>

									</form>

									<form action="{{route('tickets.update',['ticket'=>$ticket->id])}}">
										@csrf
										@method('PUT')

										<input type="hidden" name="resource_id" value="{{$ticket_resource->id}}">
										<input type="hidden" name="actionBtn" value="rejected">
										<button class="btn btn-danger btn-block">Reject</button>
									</form>
										
								</td> --}}
							</tr>

						@endforeach
					</tbody>
					
				</table>
			</div>

		</div>
	</div>	

				
				

</div>		

	
@endsection