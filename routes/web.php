<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

// Route::put('tickets/{ticket}/updateRequest', 'TicketController@resourceRequestUpdate');
Route::delete('resource_categories/deleteAll', 'ResourceCategoryController@deleteAll')->name('resource_categories.deleteAll');

Route::delete('/temp/clear','TempController@clearTemp')->name('temp.clear');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('si_resource', 'SIResourcesController');
Route::resource('temp','TempController');
Route::resource('resource_categories', 'ResourceCategoryController');
Route::resource('projects', 'ProjectController');
Route::resource('tickets','TicketController');
